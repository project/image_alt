<?php

namespace Drupal\image_alt_svg\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;
use Drupal\image_alt\Plugin\Field\FieldWidget\ImageAltPersistWidget;
use Drupal\svg_image\Plugin\Field\FieldWidget\SvgImageWidget;
use Drupal\media\MediaInterface;

/**
 * Plugin implementation of the 'svg_image_alt_persist' widget.
 *
 * Persists the alt on file replacement.
 *
 * @FieldWidget(
 *   id = "svg_image_alt_persist",
 *   label = @Translation("SVG Image alt persist"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class SvgImageAltPersistWidget extends SvgImageWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $items->getParent()->getEntity();
    // @todo
    //   - generalize to other content entities than Media
    //   - store the fields by name and support multiple values
    if (!$entity instanceof MediaInterface) {
      $form_state->setError($element, $this->t('The <em>Image alt persist</em> widget only supports Media entities.'));
    }
    $form['#private_temp_store_key'] = ImageAltPersistWidget::privateTempStoreKey($entity);
    return $element;
  }

  /**
   * Form API callback: Processes a image_image field element.
   *
   * Expands the image_image type to include the alt and title fields.
   *
   * This method is assigned as a #process callback in formElement() method.
   *
   * @param array $element
   *   Original form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param array $form
   *   Form.
   *
   * @return array
   *   Processed form element.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public static function process($element, FormStateInterface $form_state, $form) {
    $item = $element['#value'];
    $item['fids'] = $element['fids']['#value'];

    $element['#theme'] = 'image_widget';

    // Add the image preview.
    if (!empty($element['#files']) && $element['#preview_image_style']) {
      $file = reset($element['#files']);

      $variables = svg_image_get_image_file_dimensions($file);

      $variables['style_name'] = $element['#preview_image_style'];
      $variables['uri'] = $file->getFileUri();

      // Add a custom preview for SVG file.
      if (svg_image_is_file_svg($file)) {
        $element['preview'] = [
          '#weight' => -10,
          '#theme' => 'image',
          '#width' => $variables['width'],
          '#height' => $variables['height'],
          '#uri' => $variables['uri'],
        ];
      }
      else {
        $element['preview'] = [
          '#weight' => -10,
          '#theme' => 'image_style',
          '#width' => $variables['width'],
          '#height' => $variables['height'],
          '#style_name' => $variables['style_name'],
          '#uri' => $variables['uri'],
        ];
      }

      // Store the dimensions in the form so the file doesn't have to be
      // accessed again. This is important for remote files.
      $element['width'] = [
        '#type' => 'hidden',
        '#value' => $variables['width'],
      ];
      $element['height'] = [
        '#type' => 'hidden',
        '#value' => $variables['height'],
      ];
    }
    elseif (!empty($element['#default_image'])) {
      $defaultImage = $element['#default_image'];
      $file = File::load($defaultImage['fid']);
      if (!empty($file)) {
        $element['preview'] = [
          '#weight' => -10,
          '#theme' => 'image_style',
          '#width' => $defaultImage['width'],
          '#height' => $defaultImage['height'],
          '#style_name' => $element['#preview_image_style'],
          '#uri' => $file->getFileUri(),
        ];
      }
    }

    $image_alt = isset($item['alt']) ? $item['alt'] : '';
    if (array_key_exists('#private_temp_store_key', $form)
        && !empty($form['#private_temp_store_key'])
    ) {
      /** @var \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store */
      $temp_store = \Drupal::service('tempstore.private');
      $store = $temp_store->get('image_alt');
      $temp_store_key = $form['#private_temp_store_key'];
      $temporary_alt = $store->get($temp_store_key);
      if (isset($item['alt'])) {
        $store->set($temp_store_key, $item['alt']);
      }
      elseif (!empty($temporary_alt)) {
        $image_alt = $temporary_alt;
      }
    }

    // Add the additional alt and title fields.
    $element['alt'] = [
      '#title' => t('Alternative text'),
      '#type' => 'textfield',
      '#default_value' => $image_alt,
      '#description' => t('Short description of the image used by screen readers and displayed when the image is not loaded. This is important for accessibility.'),
        // @see https://www.drupal.org/node/465106#alt-text
      '#maxlength' => 512,
      '#weight' => -12,
      '#access' => (bool) $item['fids'] && $element['#alt_field'],
      '#required' => $element['#alt_field_required'],
      '#element_validate' => $element['#alt_field_required'] == 1 ? [[get_called_class(), 'validateRequiredFields']] : [],
    ];
    $element['title'] = [
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => isset($item['title']) ? $item['title'] : '',
      '#description' => t('The title is used as a tool tip when the user hovers the mouse over the image.'),
      '#maxlength' => 1024,
      '#weight' => -11,
      '#access' => (bool) $item['fids'] && $element['#title_field'],
      '#required' => $element['#title_field_required'],
      '#element_validate' => $element['#title_field_required'] == 1 ? [[get_called_class(), 'validateRequiredFields']] : [],
    ];

    // Process the parent of ImageWidget.
    return FileWidget::process($element, $form_state, $form);
  }

}
