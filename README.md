# Image alt

Provides
- a field widget and field formatter (Image alt) to optionally edit or display
 the Image alt separately from the File.
- a field widget to optionally persist the alt content on file replace
 (Image alt persist). - introduced in 1.1.x release.

**Use cases**
- When a View must expose the alt attribute only, for review or inline editing
 after say, a migration.
- When the alt does not need to be replaced while changing the file.

**Image alt widget and formatter**
- The field widget can be used as inline editable Views with e.g.
[Views Entity Form Field](https://www.drupal.org/project/views_entity_form_field)
- The field formatter can be used to extend the Media view and display
alt without the need of using field rewriting / custom template.

**Image alt persist**
The Image alt persist is currently limited to Media entities.
A submodule exists for the [SVG Image](https://www.drupal.org/project/svg_image)
module.
