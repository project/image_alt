# Image alt SVG

Provides the _Image alt persist_ field widget
for the [SVG Image](https://www.drupal.org/project/svg_image) module.

The SVG Image module fully overrides the core Image field widget.
https://git.drupalcode.org/project/svg_image/-/blob/8.x-1.x/src/Plugin/Field/FieldWidget/SvgImageWidget.php#L16-20
So instead of inheriting from the `ImageWidget`, this field widget
inherits from the `SvgImageWidget`.
