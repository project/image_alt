<?php

namespace Drupal\image_alt\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'image_alt' formatter.
 *
 * @FieldFormatter(
 *   id = "image_alt",
 *   label = @Translation("Image alt"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ImageAltFormatter extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    if (empty($images = $this->getEntitiesToView($items, $langcode))) {
      // Early opt-out if the field is empty.
      return $elements;
    }

    $alt_values = [];
    /** @var \Drupal\image\Plugin\Field\FieldType\ImageItem $item */
    foreach ($items as $item) {
      $item_id = $item->get('target_id')->getValue();
      $alt_values[$item_id] = $item->get('alt')->getValue();
    }

    /** @var \Drupal\file\FileInterface[] $images */
    foreach ($images as $delta => $image) {
      $image_alt = $alt_values[$image->id()];
      $cacheability = CacheableMetadata::createFromObject($image);
      if ($image_alt) {
        $cacheability->addCacheableDependency(CacheableMetadata::createFromObject($image_alt));
      }
      $elements[$delta] = ['#markup' => $image_alt];
      $cacheability->applyTo($elements[$delta]);
    }
    return $elements;
  }

}
